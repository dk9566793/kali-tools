---
Title: dbeaver
Homepage: http://dbeaver.jkiss.org/
Repository: https://gitlab.com/kalilinux/packages/dbeaver
Architectures: amd64
Version: 6.0.3-0kali1
Metapackages: kali-linux-everything kali-linux-large 
Icon: images/dbeaver-logo.svg
PackagesInfo: |
 ### dbeaver
 
  This package contains DBeaver Community Edition. It's Free multi-platform
  database tool for developers, SQL programmers, database administrators and
  analysts. Supports all popular databases: MySQL, PostgreSQL, SQLite, Oracle,
  DB2, SQL Server, Sybase, Teradata, Cassandra.
 
 **Installed size:** `66.71 MB`  
 **How to install:** `sudo apt install dbeaver`  
 
 {{< spoiler "Dependencies:" >}}
 * default-jre
 * libc6 
 * libglib2.0-0 
 * libsecret-1-0 
 {{< /spoiler >}}
 
 ##### dbeaver
 
 
 ```
 root@kali:~# dbeaver -h
 Dbeaver:
 An error has occurred. See the log file
 /root/.dbeaver4/.metadata/.log.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
